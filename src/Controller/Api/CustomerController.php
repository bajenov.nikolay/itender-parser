<?php

namespace App\Controller\Api;

use App\Common\Tools\JsonResponseTool;
use App\Services\Customer\Push\PushService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class CustomerController extends AbstractController
{
    private PushService $pushService;

    public function __construct(PushService $pushService)
    {
        $this->pushService = $pushService;
    }

    public function setPushToken(Request $request): JsonResponse
    {
        $this->pushService->setPushToken($this->getUser(), $request->request->get('push_token'));

        return JsonResponseTool::success([
            'result' => 'ok',
            'time' => time()
        ]);
    }



}
