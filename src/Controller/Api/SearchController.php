<?php

namespace App\Controller\Api;

use App\Common\Tools\JsonResponseTool;
use App\Services\Search\Exception\PayloadPerformingError;
use App\Services\Search\Payload\FilterPayload;
use App\Services\Search\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


class SearchController extends AbstractController
{

    private SearchService $searchService;

    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    public function search(Request $request)
    {
        try {
            $payload = $this->performPayload($request);
            $results = $this->searchService->filter($payload);


            return JsonResponseTool::success([
                'data' => $results
            ]);

        } catch (PayloadPerformingError $exception) {
            return JsonResponseTool::error('invalid_request', [
                'message' => 'Missing parameters',
            ], 400);
        }
    }

    private function performPayload(Request $request): FilterPayload
    {
        $payload = new FilterPayload();

        if ($request->request->has('filter') && !is_array($filter = $request->request->get('filter')))
        {
            throw new PayloadPerformingError('Payload performing error');
        }

        if (isset($filter['locality_id']))
        {
            $payload->setLocalityId((int) $filter['locality_id']);
        }

        if (isset($filter['procurement_format']))
        {
            $payload->setProcurementFormat((int) $filter['procurement_format']);
        }

        if (isset($filter['procurement_method']))
        {
            $payload->setProcurementMethod((int) $filter['procurement_method']);
        }

        if (isset($filter['ownership_type_id']))
        {
            $payload->setOwnershipType((int) $filter['ownership_type_id']);
        }

        if (isset($filter['phrase']))
        {
            $payload->setPhrase((string) $filter['phrase']);
        }

        if (isset($filter['page']))
        {
            $payload->setPage((int) $filter['page']);
        }

        return $payload;

    }



}
