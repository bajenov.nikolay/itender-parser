<?php

namespace App\Controller\Api;

use App\Common\Tools\JsonResponseTool;
use App\Services\Customer\Auth\AuthService;
use App\Services\Customer\Auth\Payload\AnonymousCredentialsPayload;
use App\Services\Customer\Auth\Payload\SocialCredentialsPayload;
use App\Validation\AnonymousLoginValidation;
use App\Validation\Exception\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends AbstractController
{

    private AnonymousLoginValidation $anonymousLoginValidation;
    /**
     * @var AuthService
     */
    private AuthService $authService;

    public function __construct(AnonymousLoginValidation $anonymousLoginValidation, AuthService $authService)
    {
        $this->anonymousLoginValidation = $anonymousLoginValidation;
        $this->authService = $authService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function anonymousLogin(Request $request): JsonResponse
    {
        try {
            $this->anonymousLoginValidation->validate($request->request->all());
        } catch (ValidationException $exception) {
            return $this->anonymousLoginValidation->responseJsonErrors();
        }

        $payload = new AnonymousCredentialsPayload(
            $request->request->get('platform'),
            $request->request->get('device_id')
        );

        $customer = $this->authService->authenticate($payload);

        return JsonResponseTool::success([
            'social_network' => $customer->getSocialNetworkDescription(),
            'api_token' => $customer->getApiToken(),
            'push_token' => $customer->getPushToken()
        ]);
    }

    public function login(Request $request)
    {
        $payload = new SocialCredentialsPayload();
        $apiToken = AuthFacade::anonymousLogin($payload);

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/LoginController.php',
        ]);
    }

}
