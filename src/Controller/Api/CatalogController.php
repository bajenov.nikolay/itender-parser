<?php

namespace App\Controller\Api;

use App\Common\Tools\JsonResponseTool;
use App\Services\Catalog\CatalogService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class CatalogController extends AbstractController
{

    private CatalogService $catalogService;

    public function __construct(CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;
    }

    public function all()
    {
        $localities = $this->catalogService->getLocalities();
        $procurementFormats = $this->catalogService->getProcurementFormats();
        $procurementMethods = $this->catalogService->getProcurementMethods();
        $ownershipTypes = $this->catalogService->getOwnershipTypes();

        return JsonResponseTool::success([
            'localities' => $localities,
            'procurementFormats' => $procurementFormats,
            'procurementMethods' => $procurementMethods,
            'ownershipTypes' => $ownershipTypes,
        ]);
    }

    public function localities()
    {
        $localities = $this->catalogService->getLocalities();

        return JsonResponseTool::success($localities);
    }

    public function procurementFormats()
    {
        $procurementFormats = $this->catalogService->getProcurementFormats();

        return JsonResponseTool::success($procurementFormats);
    }

    public function procurementMethods()
    {
        $procurementMethods = $this->catalogService->getProcurementMethods();

        return JsonResponseTool::success($procurementMethods);
    }

    public function ownershipTypes()
    {
        $ownershipTypes = $this->catalogService->getOwnershipTypes();

        return JsonResponseTool::success($ownershipTypes);
    }

}
