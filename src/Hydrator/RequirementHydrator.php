<?php


namespace App\Hydrator;


use App\Entity\Requirement;

class RequirementHydrator extends BaseHydrator
{
    public static function hydrate(string $title): Requirement
    {
        return new Requirement($title);
    }
}