<?php


namespace App\Hydrator;


use App\Entity\Lot;
use App\Services\Parser\Assembler\GovKg\Model\Tender\LotItem;

class LotHydrator extends BaseHydrator
{
    public static function hydrate(LotItem $lotItem, int $tenderId): Lot
    {
        return new Lot(
            $tenderId,
            $lotItem->getNumber(),
            $lotItem->getTitle(),
            $lotItem->getAmount(),
            $lotItem->getDeliveryAddress(),
            $lotItem->getDeliveryConditions()
        );
    }
}