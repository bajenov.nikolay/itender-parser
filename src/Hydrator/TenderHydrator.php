<?php


namespace App\Hydrator;


use App\Entity\Company;
use App\Entity\ProcurementFormat;
use App\Entity\ProcurementMethod;
use App\Entity\Tender;
use App\Services\Parser\Assembler\GovKg\Model\Tender\TenderItem;

class TenderHydrator extends BaseHydrator
{
    public static function hydrate(TenderItem $tenderItem, Company $company, ProcurementFormat $procurementFormat, ProcurementMethod $procurementMethod, string $lang)
    {
        return new Tender(
            $company->getId(),
            $lang,
            $procurementFormat->getId(),
            $procurementMethod->getId(),
            $tenderItem->getId(),
            $tenderItem->getEstimatedAmount(),
            $tenderItem->getTitle(),
            $tenderItem->getPublishDate(),
            $tenderItem->getNeedToApply()

        );
    }
}