<?php


namespace App\Hydrator;

use App\Entity\OwnershipType;

class OwnershipTypeHydrator extends BaseHydrator
{
    public static function hydrate(string $ownerShipTypeStr): OwnershipType
    {
        return new OwnershipType($ownerShipTypeStr);
    }
}