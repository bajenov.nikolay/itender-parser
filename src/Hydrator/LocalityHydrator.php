<?php


namespace App\Hydrator;


use App\Entity\Locality;

class LocalityHydrator extends BaseHydrator
{
    public static function hydrate(string $locality): Locality
    {
        return new Locality($locality);
    }
}