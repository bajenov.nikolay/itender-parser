<?php


namespace App\Hydrator;


use App\Entity\Locality;
use App\Entity\OwnershipType;
use App\Services\Parser\Assembler\GovKg\Model\Tender\Company;
use App\Entity\Company as CompanyEntity;

class CompanyHydrator extends BaseHydrator
{
    public static function hydrate(Company $company, OwnershipType $ownershipType, Locality $locality): CompanyEntity
    {
        return new CompanyEntity(
            $ownershipType->getId(),
            $locality->getId(),
            $company->getName(),
            $company->getInn(),
            $company->getAddress(),
            $company->getPhones(),
            $company->getBank(),
            $company->getCheckingAccount(),
            $company->getBik()
        );
    }
}