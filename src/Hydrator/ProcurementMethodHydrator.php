<?php


namespace App\Hydrator;

use App\Entity\ProcurementMethod;

class ProcurementMethodHydrator extends BaseHydrator
{
    public static function hydrate(string $procurementFormat): ProcurementMethod
    {
        return new ProcurementMethod($procurementFormat);
    }
}