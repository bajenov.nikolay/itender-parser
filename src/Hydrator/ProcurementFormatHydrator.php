<?php


namespace App\Hydrator;

use App\Entity\ProcurementFormat;

class ProcurementFormatHydrator extends BaseHydrator
{
    public static function hydrate(string $procurementFormat): ProcurementFormat
    {
        return new ProcurementFormat($procurementFormat);
    }
}