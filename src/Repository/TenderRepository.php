<?php


namespace App\Repository;


use App\Entity\Tender;
use App\Services\Search\Payload\FilterPayload;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

class TenderRepository extends ServiceEntityRepository
{
    private int $pageSize = 15;
    /**
     * @var PaginatorInterface
     */
    private PaginatorInterface $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Tender::class);
        $this->paginator = $paginator;
    }

    public function filter(FilterPayload $filterPayload)
    {
        $query = $this->createQueryBuilder('t')
            ->select(
                't.id as tender_id',
                't.number as tender_number',
                't.lang as tender_lang',
                't.title as tender_title',
                't.estimatedAmount as tender_estimated_amount',
                't.publishDate as tender_publish_date',
                't.needToApply as tender_need_to_apply',
                'c.name as company_name',
                'o.title as company_ownership_type',
                'l.title as company_locality'
            );
        $query->leftJoin('App\Entity\Company', 'c', 'WITH', 't.companyId = c.id');
        $query->leftJoin('App\Entity\OwnershipType', 'o', 'WITH', 'c.ownershipTypeId = o.id');
        $query->leftJoin('App\Entity\Locality', 'l', 'WITH', 'c.localityId = l.id');

        if ($filterPayload->hasPhrase()) {
            $query->where('t.title LIKE :phrase');
            $query->setParameter('phrase', '%' . $filterPayload->getPhrase() . '%');
        }

        if ($filterPayload->hasProcurementFormat()) {
            $query->where('t.procurementFormatId = :procurementFormatId');
            $query->setParameter('procurementFormatId', $filterPayload->getProcurementFormat());
        }

        if ($filterPayload->hasProcurementMethod()) {
            $query->where('t.procurementMethodId = :procurementMethodId');
            $query->setParameter('procurementMethodId', $filterPayload->getProcurementMethod());
        }

        if ($filterPayload->hasLocalityId()) {
            $query->where('c.localityId = :localityId');
            $query->setParameter('localityId', $filterPayload->getLocalityId());
        }

        if ($filterPayload->hasOwnershipType()) {
            $query->where('c.ownershipTypeId = :ownershipType');
            $query->setParameter('ownershipType', $filterPayload->getOwnershipType());
        }

        $query->orderBy('t.id', 'DESC');

        return $this->paginator->paginate(
            $query->getQuery(),
            $filterPayload->getPage(),
            $this->pageSize
        )->getItems();

    }

}