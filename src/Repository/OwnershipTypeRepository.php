<?php


namespace App\Repository;


use App\Entity\OwnershipType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class OwnershipTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OwnershipType::class);
    }

    public function all()
    {
        return $this->createQueryBuilder('c')
            ->select('c.id', 'c.title')
            ->where('c.deletedAt is NULL')
            ->orderBy('c.title', 'ASC')
            ->getQuery()
            ->getResult();
    }
}