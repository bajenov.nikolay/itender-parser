<?php


namespace App\Repository;


use App\Entity\Lot;
use App\Entity\ProcurementMethod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProcurementMethodRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcurementMethod::class);
    }

    public function all()
    {
        return $this->createQueryBuilder('c')
            ->select('c.id', 'c.title')
            ->where('c.deletedAt is NULL')
            ->orderBy('c.title', 'ASC')
            ->getQuery()
            ->getResult();
    }
}