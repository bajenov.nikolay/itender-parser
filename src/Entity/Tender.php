<?php


namespace App\Entity;

use DateTimeImmutable;

class Tender extends BaseEntity
{
    private int $id;
    private string $title;
    private int $number;
    private float $estimatedAmount;
    private DateTimeImmutable $publishDate;
    private ?DateTimeImmutable $needToApply;
    private int $companyId;
    private int $procurementFormatId;
    private int $procurementMethodId;
    private string $lang;


    public function __construct(
        int $companyId,
        string $lang,
        int $procurementFormatId,
        int $procurementMethodId,
        int $number,
        float $estimatedAmount,
        string $title,
        DateTimeImmutable $publishDate,
        ?DateTimeImmutable $needToApply = null
    )
    {
        parent::__construct();

        $this->title = $title;
        $this->number = $number;
        $this->estimatedAmount = $estimatedAmount;
        $this->publishDate = $publishDate;
        $this->needToApply = $needToApply;
        $this->companyId = $companyId;
        $this->lang = $lang;
        $this->procurementFormatId = $procurementFormatId;
        $this->procurementMethodId = $procurementMethodId;
    }

    public function getLang(): string
    {
        return $this->lang;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    public function getProcurementFormatId(): int
    {
        return $this->procurementFormatId;
    }

    public function getProcurementMethodId(): int
    {
        return $this->procurementMethodId;
    }

    public function getEstimatedAmount(): float
    {
        return $this->estimatedAmount;
    }

    public function getPublishDate(): DateTimeImmutable
    {
        return $this->publishDate;
    }

    public function getNeedToApply(): string
    {
        return $this->needToApply;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
}