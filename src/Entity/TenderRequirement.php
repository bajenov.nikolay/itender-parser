<?php


namespace App\Entity;

class TenderRequirement extends BaseEntity
{
    private int $tenderId;
    private int $requirementId;
    private string $description;

    public function __construct(int $tenderId, int $requirementId, string $description)
    {
        parent::__construct();

        $this->tenderId = $tenderId;
        $this->requirementId = $requirementId;
        $this->description = $description;
    }

    public function getTenderId(): int
    {
        return $this->tenderId;
    }

    public function getRequirementId(): int
    {
        return $this->requirementId;
    }

    public function getDescription(): string
    {
        return $this->description;
    }



}