<?php


namespace App\Entity;


class Locality extends BaseEntity
{
    private int $id;
    private string $title;

    public function __construct(string $title)
    {
        parent::__construct();

        $this->title = $title;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
}