<?php


namespace App\Entity;

class Lot extends BaseEntity
{
    private int $id;
    private int $tenderId;
    private string $number;
    private string $title;
    private float $amount;
    private ?string $deliveryAddress;
    private ?string $deliveryConditions;



    public function __construct(
        int $tenderId,
        string $number,
        string $title,
        float $amount,
        ?string $deliveryAddress,
        ?string $deliveryConditions
    )
    {
        parent::__construct();

        $this->tenderId = $tenderId;
        $this->number = $number;
        $this->title = $title;
        $this->amount = $amount;
        $this->deliveryAddress = $deliveryAddress;
        $this->deliveryConditions = $deliveryConditions;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTenderId(): int
    {
        return $this->tenderId;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function getDeliveryAddress(): ?string
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryAddress(?string $deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getDeliveryConditions(): ?string
    {
        return $this->deliveryConditions;
    }

    public function setDeliveryConditions(?string $deliveryConditions): void
    {
        $this->deliveryConditions = $deliveryConditions;
    }
}