<?php


namespace App\Entity;

use DateTime;

class BaseEntity
{
    protected DateTime $createdAt;
    protected DateTime $updatedAt;
    protected ?DateTime $deletedAt;

    protected function __construct()
    {
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    protected function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    protected function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    protected function getDeletedAt(): ?DateTime
    {
        return $this->deletedAt;
    }
}