<?php

namespace App\Entity;

use App\Enum\SocialNetwork;
use Symfony\Component\Security\Core\User\UserInterface;

class Customer extends BaseEntity implements UserInterface
{

    public function __construct()
    {
        parent::__construct();
    }

    private int $id;
    private ?string $email = null;
    private array $roles = [];
    private string $apiToken;
    private ?string $socialNetwork = null;
    private ?string $socialNetworkId = null;
    private ?string $pushToken = null;
    private ?string $platform = null;
    private ?string $deviceId = null;
    private ?string $name = null;
    private ?string $phone = null;
    private ?string $password = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_CUSTOMER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getApiToken(): string
    {
        return $this->apiToken;
    }

    public function setApiToken(string $apiToken): void
    {
        $this->apiToken = $apiToken;
    }

    public function getPushToken(): ?string
    {
        return $this->pushToken;
    }

    public function setPushToken(?string $pushToken): void
    {
        $this->pushToken = $pushToken;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    public function getSocialNetwork(): ?string
    {
        return $this->socialNetwork;
    }

    public function getSocialNetworkDescription(): ?string
    {
        return SocialNetwork::getValueByCode($this->socialNetwork);
    }

    public function setSocialNetwork(?string $socialNetwork): void
    {
        $this->socialNetwork = $socialNetwork;
    }

    public function getSocialNetworkId(): ?string
    {
        return $this->socialNetworkId;
    }

    public function setSocialNetworkId(?string $socialNetworkId): void
    {
        $this->socialNetworkId = $socialNetworkId;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(?string $platform): void
    {
        $this->platform = $platform;
    }

    public function getDeviceId(): ?string
    {
        return $this->deviceId;
    }

    public function setDeviceId(?string $deviceId): void
    {
        $this->deviceId = $deviceId;
    }


    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        // not needed for apps that do not check user passwords
    }

    public function getSalt()
    {
        // not needed for apps that do not check user passwords
    }

    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }


}
