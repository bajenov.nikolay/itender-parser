<?php


namespace App\Entity;

class OwnershipType extends BaseEntity
{
    private int $id;
    private string $title;

    public function __construct(string $title)
    {
        $this->title = $title;
        parent::__construct();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }


}