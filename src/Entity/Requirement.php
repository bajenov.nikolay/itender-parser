<?php


namespace App\Entity;

class Requirement extends BaseEntity
{
    private int $id;
    private string $title;

    public function __construct(string $title)
    {
        parent::__construct();

        $this->title = $title;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

}