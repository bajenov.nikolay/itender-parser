<?php


namespace App\Entity;

class Company extends BaseEntity
{
    private int $id;
    private int $ownershipTypeId;
    private int $localityId;
    private string $name;
    private int $inn;
    private string $address;
    private ?string $phone;
    private string $bank;
    private int $checkingAccount;
    private int $bik;


    public function __construct(
        int $ownershipTypeId,
        int $localityId,
        string $name,
        int $inn,
        string $address,
        string $phone,
        string $bank,
        int $checkingAccount,
        int $bik
    )
    {
        parent::__construct();

        $this->ownershipTypeId = $ownershipTypeId;
        $this->localityId = $localityId;
        $this->name = $name;
        $this->inn = $inn;
        $this->address = $address;
        $this->phone = $phone;
        $this->bank = $bank;
        $this->checkingAccount = $checkingAccount;
        $this->bik = $bik;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOwnershipTypeId(): int
    {
        return $this->ownershipTypeId;
    }

    public function setOwnershipTypeId(int $ownershipTypeId): void
    {
        $this->ownershipTypeId = $ownershipTypeId;
    }

    public function getLocalityId(): int
    {
        return $this->localityId;
    }

    public function setLocalityId(int $localityId): void
    {
        $this->localityId = $localityId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getInn(): int
    {
        return $this->inn;
    }

    public function setInn(int $inn): void
    {
        $this->inn = $inn;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    public function getBank(): string
    {
        return $this->bank;
    }

    public function setBank(string $bank): void
    {
        $this->bank = $bank;
    }

    public function getCheckingAccount(): int
    {
        return $this->checkingAccount;
    }

    public function setCheckingAccount(int $checkingAccount): void
    {
        $this->checkingAccount = $checkingAccount;
    }

    public function getBik(): int
    {
        return $this->bik;
    }

    public function setBik(int $bik): void
    {
        $this->bik = $bik;
    }



}