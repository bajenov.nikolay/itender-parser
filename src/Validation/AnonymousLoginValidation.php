<?php


namespace App\Validation;


use App\Enum\Platform;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;


class AnonymousLoginValidation extends BaseValidation
{
    public function __construct()
    {
        $this->constraint = new Collection([
            'platform' => new Choice(Platform::toArray()),
            'device_id' => new Length([
                'min' => 10,
                'max' => 255,
                'allowEmptyString' => false,
            ])
        ]);
    }







}