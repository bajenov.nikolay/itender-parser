<?php


namespace App\Validation;

use App\Common\Tools\JsonResponseTool;
use App\Enum\ApiResponseTypes;
use App\Validation\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseValidation
{
    protected ?Collection $constraint ;
    protected ?ConstraintViolationListInterface $constraintViolation;
    protected ? ValidatorInterface $validator;
    protected array $input;

    public function responseJsonErrors()
    {
        $data = [];

        foreach ($this->constraintViolation as $k => $constraintViolation)
        {
            $data[substr($constraintViolation->getPropertyPath(),1, -1)] = $constraintViolation->getMessage();
        }

        return JsonResponseTool::error(ApiResponseTypes::VALIDATION(), $data, 422);
    }

    public function validate(array $input)
    {
        $this->input = $input;
        $this->validator = Validation::createValidator();

        $this->constraintViolation = $this->validator->validate($input, $this->constraint);

        if ($this->constraintViolation->count())
        {
            throw new ValidationException('Validation error');
        }
    }


}