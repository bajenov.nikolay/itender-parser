<?php


namespace App\Services\Search\Payload;


class FilterPayload
{
    private ?string $phrase = null;
    private ?int $localityId = null;
    private ?int $procurementFormat = null;
    private ?int $procurementMethod = null;
    private ?int $ownershipType = null;
    private int $page = 1;

    public function getPhrase(): ?string
    {
        return $this->phrase;
    }

    public function setPhrase(?string $phrase): void
    {
        $this->phrase = $phrase;
    }

    public function getLocalityId(): ?int
    {
        return $this->localityId;
    }

    public function setLocalityId(?int $localityId): void
    {
        $this->localityId = $localityId;
    }

    public function getProcurementFormat(): ?int
    {
        return $this->procurementFormat;
    }

    public function setProcurementFormat(?int $procurementFormat): void
    {
        $this->procurementFormat = $procurementFormat;
    }

    public function getProcurementMethod(): ?int
    {
        return $this->procurementMethod;
    }

    public function setProcurementMethod(?int $procurementMethod): void
    {
        $this->procurementMethod = $procurementMethod;
    }

    public function getOwnershipType(): ?int
    {
        return $this->ownershipType;
    }

    public function setOwnershipType(?int $ownershipType): void
    {
        $this->ownershipType = $ownershipType;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    public function hasPhrase(): bool
    {
        return !is_null($this->phrase);
    }

    public function hasProcurementFormat(): bool
    {
        return !is_null($this->procurementFormat);
    }

    public function hasProcurementMethod(): bool
    {
        return !is_null($this->procurementMethod);
    }

    public function hasLocalityId(): bool
    {
        return !is_null($this->localityId);
    }

    public function hasOwnershipType(): bool
    {
        return !is_null($this->ownershipType);
    }




}