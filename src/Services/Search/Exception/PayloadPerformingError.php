<?php


namespace App\Services\Search\Exception;


use RuntimeException;

class PayloadPerformingError extends RuntimeException
{

}