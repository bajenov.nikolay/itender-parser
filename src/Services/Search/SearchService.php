<?php


namespace App\Services\Search;


use App\Repository\TenderRepository;
use App\Services\Search\Payload\FilterPayload;

class SearchService
{
    private TenderRepository $tenderRepository;

    public function __construct(TenderRepository $tenderRepository)
    {
        $this->tenderRepository = $tenderRepository;
    }

    public function filter(FilterPayload $filterPayload)
    {
        return $this->tenderRepository->filter($filterPayload);
    }
}