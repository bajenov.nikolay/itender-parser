<?php


namespace App\Services\Parser\Assembler\GovKg\Validator;



use App\Services\Parser\Assembler\GovKg\Rule\CommonDataRules;

class CommonDataValidator extends BaseValidator
{

    public function validateArray(array $commonData)
    {
        $rules = CommonDataRules::rules();

        foreach ($rules as $key => $value)
        {
            if (!isset($commonData[0][$key][0]))
            {
                $this->throwMissingRequiredArrayKey((string) $key);
            }

            if ($commonData[0][$key][0] !== $value)
            {
                $this->throwArrayValueDoesNotMatchPattern($value, $commonData[0][$key][0]);
            }
        }
    }
}