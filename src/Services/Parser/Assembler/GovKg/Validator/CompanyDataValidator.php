<?php


namespace App\Services\Parser\Assembler\GovKg\Validator;


use App\Services\Parser\Assembler\GovKg\Rule\CompanyDataRules;

class CompanyDataValidator extends BaseValidator
{
    public function validateArray(array $companyData)
    {
        $rules = CompanyDataRules::rules();


        foreach ($rules as $key => $value)
        {
            if (!isset($companyData[$key]))
            {
                $this->throwMissingRequiredArrayKey((string) $key);
            }

            if ($companyData[$key][0] !== $value)
            {
                $this->throwArrayValueDoesNotMatchPattern($value, $companyData[$key][0]);
            }
        }
    }
}