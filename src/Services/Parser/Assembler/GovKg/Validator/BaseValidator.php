<?php


namespace App\Services\Parser\Assembler\GovKg\Validator;

use App\Services\Parser\Assembler\Exception\ArrayValueDoesNotMatchPattern;
use App\Services\Parser\Assembler\Exception\MissingLots;
use App\Services\Parser\Assembler\Exception\MissingRequiredArrayKey;

abstract class BaseValidator
{
    protected function throwMissingRequiredArrayKey(string $key)
    {
        throw new MissingRequiredArrayKey('Missing required array key: ' . $key);
    }

    protected function throwArrayValueDoesNotMatchPattern(string $pattern, string $original)
    {
        throw new ArrayValueDoesNotMatchPattern('Array value does not match pattern. Pattern:' . $pattern . ' Value: ' . $original);
    }

    protected function throwMissingLots(string $number)
    {
        throw new MissingLots('There are not lots. ID: ' . $number);
    }
}