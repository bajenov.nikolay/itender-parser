<?php


namespace App\Services\Parser\Assembler\GovKg\Validator;


class LotsValidator extends BaseValidator
{
    public function validateArray(array $lots, string $tenderNUmber)
    {
        if (!isset($lots[0]) || 0 === count($lots[0]))
        {
            $this->throwMissingLots($tenderNUmber);
        }
    }
}