<?php


namespace App\Services\Parser\Assembler\GovKg\Rule;


use MyCLabs\Enum\Enum;

class CommonDataRules extends Enum
{
    private const FIELD_NUMBER = 'Номер';
    private const FIELD_NAME_OF_PURCHASE = 'Наименование закупки';
    private const FIELD_PURCHASE_FORMAT = 'Формат закупок';
    private const FIELD_PURCHASE_METHOD = 'Метод закупок';
    private const FIELD_ESTIMATED_AMOUNT = 'Планируемая сумма';
    private const FIELD_PUBLISH_DATE = 'Дата публикации';
    private const FIELD_NEED_TO_APPLY = 'Срок подачи конкурсных заявок';


    public static function rules(): array
    {
        return [
            0 => self::FIELD_NUMBER,
            1 => self::FIELD_NAME_OF_PURCHASE,
            3 => self::FIELD_PURCHASE_FORMAT,
            4 => self::FIELD_PURCHASE_METHOD,
            5 => self::FIELD_ESTIMATED_AMOUNT,
            6 => self::FIELD_PUBLISH_DATE,
            8 => self::FIELD_NEED_TO_APPLY
        ];
    }
}