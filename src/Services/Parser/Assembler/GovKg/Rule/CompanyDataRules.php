<?php


namespace App\Services\Parser\Assembler\GovKg\Rule;


use MyCLabs\Enum\Enum;

class CompanyDataRules extends Enum
{
    private const FIELD_NAME_OF_COMPANY = 'Наименование организации';
    private const FIELD_TYPE_OF_OWNERSHIP = 'Форма собственности';
    private const FIELD_INN = 'ИНН организации';
    private const FIELD_LOCALITY = 'Населённый пункт';
    private const FIELD_ACTUAL_ADDRESS = 'Фактический адрес';
    private const FIELD_WORK_PHONE = 'Рабочий телефон';
    private const FIELD_BANK = 'Банк';
    private const FIELD_CHECKING_ACCOUNT = 'Р/счет';
    private const FIELD_BIK = 'БИК';

    public static function rules(): array
    {
        return [
            0 => self::FIELD_NAME_OF_COMPANY,
            1 => self::FIELD_TYPE_OF_OWNERSHIP,
            2 => self::FIELD_INN,
            3 => self::FIELD_LOCALITY,
            4 => self::FIELD_ACTUAL_ADDRESS,
            5 => self::FIELD_WORK_PHONE,
            6 => self::FIELD_BANK,
            7 => self::FIELD_CHECKING_ACCOUNT,
            8 => self::FIELD_BIK,
        ];
    }
}