<?php


namespace App\Services\Parser\Assembler\GovKg;


use App\Common\Tools\DateTool;
use App\Common\Tools\NumberTool;
use App\Services\Parser\Assembler\GovKg\Model\Tender\Company;
use App\Services\Parser\Assembler\GovKg\Model\Tender\LotList;
use App\Services\Parser\Assembler\GovKg\Model\Tender\RequirementList;
use App\Services\Parser\Assembler\GovKg\Model\Tender\TenderList;
use App\Services\Parser\Assembler\GovKg\Validator\CommonDataValidator;
use App\Services\Parser\Assembler\GovKg\Validator\CompanyDataValidator;
use App\Services\Parser\Assembler\GovKg\Validator\LotsValidator;
use Exception;

class Assembler
{
    private CommonDataValidator $commonDataValidator;
    private CompanyDataValidator $companyDataValidator;
    private LotsValidator $lotsValidator;

    public function __construct(
        CommonDataValidator $commonDataValidator,
        CompanyDataValidator $companyDataValidator,
        LotsValidator $lotsValidator
    )
    {
        $this->commonDataValidator = $commonDataValidator;
        $this->companyDataValidator = $companyDataValidator;
        $this->lotsValidator = $lotsValidator;
    }

    public function assembleTenderList(array $latestTenders): TenderList
    {
        try {
            foreach ($latestTenders as $tender) {
                $this->commonDataValidator->validateArray($tender['commonData']);
                $this->companyDataValidator->validateArray($tender['company']);
                $this->lotsValidator->validateArray($tender['lots'], $tender['commonData'][0][0][1]);
            }

            return $this->wrapToTenderListModel($latestTenders);

        } catch (Exception $exception) {
            dd($exception->getMessage(), $exception->getFile(), $exception->getLine());
        }
    }

    private function wrapToTenderListModel(array $list): TenderList
    {
        $data = [];

        foreach ($list as $item)
        {
            $commonData = $item['commonData'][0];
            $companyData = $item['company'];
            $lots = $item['lots'][0];
            $requirements = $item['requirements'];

            $lotItems = [];
            $requirementsArray = [];

            foreach ($lots as $lot)
            {
                $lotItems[] = [
                    'tender_id' => $commonData[0][1],
                    'number' => $lot[0][1],
                    'title' => $lot[1][1],
                    'amount' => NumberTool::fromStringToFloat($lot[2][1]),
                    'deliveryAddress' => $lot[3][1],
                    'deliveryConditions' => (isset($lot[6][1])) ? $lot[6][1] : '',
                ];
            }

            $lotList = LotList::fromArray($lotItems);

            foreach ($requirements as $requirement)
            {
                $requirementsArray[] = [
                    'title' => $requirement[1],
                    'desc' => $requirement[2],
                ];
            }

            $requirementList = RequirementList::fromArray($requirementsArray);

            $company = [
                'name' => $companyData[0][1],
                'typeOfOwnership' => $companyData[1][1],
                'inn' => $companyData[2][1],
                'locality' => $companyData[3][1],
                'address' => $companyData[4][1],
                'phones' => $companyData[5][1],
                'bank' => $companyData[6][1],
                'checkingAccount' => $companyData[7][1],
                'bik' => $companyData[8][1]
            ];

            $companyObject = Company::fromArray($company);

            $data[] = [
                'id' => $commonData[0][1],
                'title' => $commonData[1][1],
                'purchaseFormat' => $commonData[3][1],
                'purchaseMethod' => $commonData[4][1],
                'estimatedAmount' => NumberTool::fromStringToFloat($commonData[5][1]),
                'publishDate' => DateTool::fromStringDateTimeImmutable($commonData[6][1]),
                'needToApply' => (isset($commonData[8][1])) ? DateTool::fromStringDateTimeImmutable($commonData[8][1]) : null,
                'lotList' => $lotList,
                'requirementList' => $requirementList,
                'company' => $companyObject
            ];
        }

        return TenderList::fromArray($data);
    }


}