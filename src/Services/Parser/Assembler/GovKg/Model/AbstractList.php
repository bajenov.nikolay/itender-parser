<?php


namespace App\Services\Parser\Assembler\GovKg\Model;

use ArrayIterator;
use Countable;
use Iterator;
use IteratorAggregate;

class AbstractList implements IteratorAggregate, Countable
{

    private array $items;

    private int $total;

    public function getIterator(): Iterator
    {
        return new ArrayIterator($this->items);
    }

    public function count(): int
    {
        return $this->total;
    }

    protected function __construct(array $items)
    {
        $this->items = $items;
        $this->total = count($items);
    }
}