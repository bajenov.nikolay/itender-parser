<?php


namespace App\Services\Parser\Assembler\GovKg\Model\Tender;


class Requirement
{
    private string $requirement;
    private string $desc;

    public function __construct(string $requirement, string $desc)
    {
        $this->requirement = $requirement;
        $this->desc = $desc;
    }

    public static function fromArray(array $data): self
    {
        return new self($data['title'], $data['desc']);
    }

    public function getRequirement(): string
    {
        return $this->requirement;
    }

    public function getDesc(): string
    {
        return $this->desc;
    }
}