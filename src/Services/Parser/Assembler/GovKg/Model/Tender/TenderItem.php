<?php


namespace App\Services\Parser\Assembler\GovKg\Model\Tender;


use DateTimeImmutable;

class TenderItem
{

    private int $id;
    private string $title;
    private string $purchaseFormat;
    private string $purchaseMethod;
    private float $estimatedAmount;
    private DateTimeImmutable $publishDate;
    private ?DateTimeImmutable $needToApply;
    private LotList $lotList;
    private RequirementList $requirementList;
    private Company $company;


    private function __construct(
        int $id,
        string $title,
        string $purchaseFormat,
        string $purchaseMethod,
        float $estimatedAmount,
        DateTimeImmutable $publishDate,
        ?DateTimeImmutable $needToApply,
        LotList $lotList,
        RequirementList $requirementList,
        Company $company
    )
    {
        $this->id = $id;
        $this->title = $title;
        $this->purchaseFormat = $purchaseFormat;
        $this->purchaseMethod = $purchaseMethod;
        $this->estimatedAmount = $estimatedAmount;
        $this->publishDate = $publishDate;
        $this->needToApply = $needToApply;
        $this->lotList = $lotList;
        $this->requirementList = $requirementList;
        $this->company = $company;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['title'],
            $data['purchaseFormat'],
            $data['purchaseMethod'],
            $data['estimatedAmount'],
            $data['publishDate'],
            $data['needToApply'],
            $data['lotList'],
            $data['requirementList'],
            $data['company'],
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPurchaseFormat(): string
    {
        return $this->purchaseFormat;
    }

    public function getPurchaseMethod(): string
    {
        return $this->purchaseMethod;
    }

    public function getEstimatedAmount(): float
    {
        return $this->estimatedAmount;
    }

    public function getPublishDate(): DateTimeImmutable
    {
        return $this->publishDate;
    }

    public function getNeedToApply(): ?DateTimeImmutable
    {
        return $this->needToApply;
    }

    public function getLotList(): LotList
    {
        return $this->lotList;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function getRequirementList(): RequirementList
    {
        return $this->requirementList;
    }



}