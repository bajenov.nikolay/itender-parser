<?php


namespace App\Services\Parser\Assembler\GovKg\Model\Tender;


class LotItem
{
    private int $tenderId;
    private string $number;
    private string $title;
    private float $amount;
    private string $deliveryAddress;
    private string $deliveryConditions;

    private function __construct(
        int $tenderId,
        string $number,
        string $title,
        float $amount,
        string $deliveryAddress,
        string $deliveryConditions
    )
    {
        $this->tenderId = $tenderId;
        $this->number = $number;
        $this->title = $title;
        $this->amount = $amount;
        $this->deliveryAddress = $deliveryAddress;
        $this->deliveryConditions = $deliveryConditions;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['tender_id'],
            $data['number'],
            $data['title'],
            $data['amount'],
            $data['deliveryAddress'],
            $data['deliveryConditions'],
        );
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getTenderId(): int
    {
        return $this->tenderId;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getDeliveryAddress(): string
    {
        return $this->deliveryAddress;
    }

    public function getDeliveryConditions(): string
    {
        return $this->deliveryConditions;
    }

}