<?php


namespace App\Services\Parser\Assembler\GovKg\Model\Tender;

use App\Services\Parser\Assembler\GovKg\Model\AbstractList;

final class TenderList extends AbstractList
{
    public static function fromArray(array $data): self
    {
        $items = array_map([TenderItem::class, 'fromArray'], $data);

        return new self($items);
    }
}