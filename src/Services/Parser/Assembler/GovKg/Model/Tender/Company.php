<?php


namespace App\Services\Parser\Assembler\GovKg\Model\Tender;


class Company
{
    private string $name;
    private string $typeOfOwnership;
    private string $inn;
    private string $locality;
    private string $address;
    private string $phones;
    private string $bank;
    private string $checkingAccount;
    private string $bik;

    public function __construct(
        string $name,
        string $typeOfOwnership,
        string $inn,
        string $locality,
        string $address,
        string $phones,
        string $bank,
        string $checkingAccount,
        string $bik
    )
    {

        $this->name = $name;
        $this->typeOfOwnership = $typeOfOwnership;
        $this->inn = $inn;
        $this->locality = $locality;
        $this->address = $address;
        $this->phones = $phones;
        $this->bank = $bank;
        $this->checkingAccount = $checkingAccount;
        $this->bik = $bik;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['name'],
            $data['typeOfOwnership'],
            $data['inn'],
            $data['locality'],
            $data['address'],
            $data['phones'],
            $data['bank'],
            $data['checkingAccount'],
            $data['bik'],
        );
    }

    public function getBank(): string
    {
        return $this->bank;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTypeOfOwnership(): string
    {
        return $this->typeOfOwnership;
    }

    public function getInn(): string
    {
        return $this->inn;
    }

    public function getLocality(): string
    {
        return $this->locality;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getPhones(): string
    {
        return $this->phones;
    }

    public function getCheckingAccount(): int
    {
        return (int) $this->checkingAccount;
    }

    public function getBik(): string
    {
        return $this->bik;
    }
}