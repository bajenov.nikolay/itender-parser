<?php


namespace App\Services\Parser\Assembler\GovKg\Model\Tender;

use App\Services\Parser\Assembler\GovKg\Model\AbstractList;

final class LotList extends AbstractList
{
    public static function fromArray(array $data): self
    {
        $items = array_map([LotItem::class, 'fromArray'], $data);
        return new self($items);
    }
}