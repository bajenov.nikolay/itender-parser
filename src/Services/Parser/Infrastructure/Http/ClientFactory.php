<?php

declare(strict_types=1);

namespace App\Services\Parser\Infrastructure\Http;

use App\Services\Parser\Infrastructure\Http\Resource\GovKg;

class ClientFactory
{
    private GovKg $govKg;

    public function __construct(GovKg $govKg)
    {
        $this->govKg = $govKg;
    }

    public function govKg(): GovKg
    {
        return $this->govKg;
    }
}