<?php


namespace App\Services\Parser\Infrastructure\Http\Resource;

use Http\Adapter\Guzzle6\Client as ClientAdapter;
use Http\Client\Common\HttpMethodsClient;
use Http\Client\Exception;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\DomCrawler\Crawler;
use App\Services\Parser\Assembler\GovKg\Assembler;


class GovKg
{
    private Assembler $assembler;
    private ContainerBagInterface $params;
    private HttpMethodsClient $http;

    public function __construct( ContainerBagInterface $params, Assembler $assembler)
    {
        $this->assembler = $assembler;
        $this->params = $params;
        $messageFactory = new GuzzleMessageFactory();
        $http = new ClientAdapter();
        $this->http = new HttpMethodsClient($http, $messageFactory);
    }

    public function parseLatestData()
    {
        $url = $this->params->get('parser')['gov_kg']['url'];

        $initialTenderList = $this->getInitialTenderList($url);

        $tenders = [];

        foreach ($initialTenderList as $tender) {
            array_push($tenders, $this->parseTender($tender));
        }

       return $this->assembler->assembleTenderList($tenders);
    }

    private function parseTender(array $tenderItem): array
    {
        $url = 'http://zakupki.gov.kg/popp/view/order/view.xhtml?id=%s';

        $response = $this->http->get(sprintf($url, $tenderItem[0]));
        $content = $response->getBody()->getContents();

        $commonData = $this->parseTenderCommonData($content);
        $lots = $this->parseTenderLots($content);
        $requirements = $this->parseTenderRequirements($content);
        $company = $this->parseCompany($tenderItem[1]['json']);


        return [
            'commonData' => $commonData,
            'lots' => $lots,
            'requirements' => $requirements,
            'company' => $company
        ];
    }

    private function parseCompany(string $json): array
    {
        try {
            $json = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            $url = $this->params->get('parser')['gov_kg']['url'];
            $response = $this->http->get($url);

            $content = $response->getBody()->getContents();
            $viewState = $this->extractViewStateValue($content);

            $formAction = $this->extractFormAction($content);

            $post = [
                'j_idt113' => 'j_idt113',
                'j_idt113:j_idt114:table_rppDD' => 10,
                'j_idt113:j_idt114_activeIndex' => '0',
                'javax.faces.ViewState' => $viewState,
                array_key_first($json) => array_key_last($json)

            ];

            $headers = [
                'Cookie' => $response->getHeaders()['Set-Cookie'][0],
                'Content-Type' => 'application/x-www-form-urlencoded'
            ];

            $response = $this->http->post('http://zakupki.gov.kg' . $formAction, $headers, http_build_query($post));

            if (200 === $response->getStatusCode())
            {
                $content = $response->getBody()->getContents();

                return (new Crawler($content))
                    ->filter('table')
                    ->first()->filter('tbody')->filter('tr')->each(function ($tr){
                        /** @var Crawler $tr */
                        return $tr->filter('td')->slice(0, 8)->each(function ($td){
                            return $td->text();
                        });
                    });
            }
        } catch (Exception $e) {

        }
    }


    private function parseTenderRequirements(string $content)
    {
        $crawler = new Crawler($content);

        /** @var Crawler $containerContentRows */
        $requirements = $crawler->filter('.container-content')->slice(3, 1);

        /** @var Crawler $table */
        $table = $requirements->filter('table tbody')->first();

        return $table->filter('tr')->each(function ($tr) {
            /** @var Crawler $tr */
            return $tr->filter('td')->each(function ($td) {
                /** @var Crawler $td */
                return $td->text();
            });
        });
    }

    private function parseTenderLots(string $content)
    {
        $crawler = new Crawler($content);

        /** @var Crawler $containerContentRows */
        $tenderLots = $crawler->filter('.container-content')->slice(2, 1);

        /** @var Crawler $tables */
        $tables = $tenderLots->filter('table tbody');

        $tables = $tables->each(function ($table) {
            return $table->filter('tr')->each(function ($tr) {
                return $tr->filter('td')->each(function ($td) {
                    /** @var Crawler $td */
                    return $td->filter('span')->each(function ($span) {
                        /** @var Crawler $span */
                        return $span->text();
                    });
                });
            });

        });

        return $tables;
    }

    private function parseTenderCommonData(string $content): array
    {
        $crawler = new Crawler($content);

        /** @var Crawler $containerContentRows */
        $tenderCommonContainerContentRows = $crawler->filter('.container-content')->slice(0, 1);

        return $tenderCommonContainerContentRows->each(function ($rows) {

            /** @var Crawler $divs */
            /** @var Crawler $rows */
            $divs = $rows->filter('.row .col-12')->slice(0, 10);

            return $divs->each(function ($div) {
                /** @var Crawler $div */
                $spans = $div->filter('span');

                return $spans->each(function ($span) {
                    /** @var Crawler $span */
                    return $span->text();
                });
            });
        });
    }

    private function getInitialTenderList(string $url): array
    {
        $response = $this->http->get($url);

        if (200 !== $response->getStatusCode()) {
            throw new RuntimeException('Resource response with code ' . $response->getStatusCode());
        }

        $content = $response->getBody()->getContents();
        return $this->extractItems($content);
    }

    private function extractItems(string $html): array
    {
        $crawler = new Crawler($html);

        $replacements = [
            '№',
            'Наименование организации',
            'Вид закупок',
            'Наименование закупки',
            'Метод закупок',
            'Планируемая сумма',
            'Дата опубликования',
            'Срок подачи конкурсных заявок',
        ];

        $trs = $crawler
            ->filter('#j_idt113')->filter('table > tbody > tr')
            ->each(function ($tr, $i) use ($replacements) {
                /** @var Crawler $tr */
                return $tr->filter('td')->slice(0, 7)->each(function ($td, $k) use ($replacements) {
                    /** @var Crawler $td */
                    if (0 === $k) {
                        $str = trim(str_replace($replacements, "", $td->text()));
                        return mb_substr($str, 6);
                    }

                    if (1 === $k) {

                        $pattern = '
                                    /
                                    \{              # { character
                                        (?:         # non-capturing group
                                            [^{}]   # anything that is not a { or }
                                            |       # OR
                                            (?R)    # recurses the entire pattern
                                        )*          # previous group zero or more times
                                    \}              # } character
                                    /x
                                    ';
                        preg_match($pattern, $td->filter('a')->attr('onclick'), $matches);
                        return [
                            'json' => str_replace("'", '"', $matches[0]),
                            'title' => str_replace($replacements, "", $td->text())
                        ];
                    }
                    return str_replace($replacements, "", $td->text());
                });

            });

        return $trs;
    }

    private function extractFormAction(string $html): string
    {
        $crawler = new Crawler($html);

        return $crawler
            ->filterXPath('//form[@id="form"]')
            ->first()
            ->extract(['action'])[0];
    }

    private function extractViewStateValue(string $html): string
    {
        $crawler = new Crawler($html);

        return $crawler
            ->filterXPath('//input[@name="javax.faces.ViewState"]')
            ->first()
            ->extract(['value'])[0];
    }
}