<?php


namespace App\Services\Parser\Facade;

use App\Services\Parser\Assembler\GovKg\Model\Tender\TenderList;

use App\Services\Parser\Infrastructure\Http\ClientFactory;

class ParserFacade
{
    private ClientFactory $factory;

    public function __construct(ClientFactory $factory)
    {
        $this->factory = $factory;
    }

    public function parseGovKg(): TenderList
    {
        return $this->factory->govKg()->parseLatestData();
    }
}