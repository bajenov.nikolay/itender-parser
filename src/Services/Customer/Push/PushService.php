<?php


namespace App\Services\Customer\Push;


use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;

class PushService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function setPushToken(Customer $customer, string $pushToken)
    {
        $customer->setPushToken($pushToken);
        $this->entityManager->persist($customer);
        $this->entityManager->flush();
    }
}