<?php


namespace App\Services\Customer\Auth\Payload;


class AnonymousCredentialsPayload implements CredentialsInterface
{
    private string $platform;
    private string $deviceId;

    public function __construct(string $platform, string $deviceId)
    {
        $this->platform = $platform;
        $this->deviceId = $deviceId;
    }

    public function getPlatform(): string
    {
        return $this->platform;
    }

    public function getDeviceId(): string
    {
        return $this->deviceId;
    }
}