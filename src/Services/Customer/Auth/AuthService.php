<?php


namespace App\Services\Customer\Auth;


use App\Entity\Customer;
use App\Enum\SocialNetwork;
use App\Repository\CustomerRepository;
use App\Services\Customer\Auth\Dto\ApiTokenDto;
use App\Services\Customer\Auth\Payload\AnonymousCredentialsPayload;
use App\Services\Customer\Auth\Payload\CredentialsInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthService
{
    private CustomerRepository $customerRepository;
    private EntityManager $entityManager;
    private UserPasswordEncoderInterface $encoder;

    public function __construct(
        CustomerRepository $customerRepository,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $encoder
    )
    {
        $this->customerRepository = $customerRepository;
        $this->entityManager = $entityManager;
        $this->encoder = $encoder;
    }


    public function authenticate(CredentialsInterface $credential): Customer
    {
        if ($credential instanceof AnonymousCredentialsPayload)
        {
            /** @var AnonymousCredentialsPayload $credential  */
            $customer = $this->customerRepository->findOneBy([
                'deviceId' => $credential->getDeviceId(),
                'platform' => $credential->getPlatform(),
                'socialNetwork' => SocialNetwork::ANONYMOUS_CODE(),
            ]);

            if (null === $customer)
            {
                $customer = $this->createAnonymousCustomer($credential->getDeviceId(), $credential->getPlatform());
            }

            return $customer;
        }

    }

    private function createAnonymousCustomer(string $deviceId, string $platform): Customer
    {

        try {

            $customer = new Customer();
            $customer->setDeviceId($deviceId);
            $customer->setPlatform($platform);
            $customer->setApiToken(md5(uniqid()));
            $customer->setSocialNetwork(SocialNetwork::ANONYMOUS_CODE());
            $customer->setSocialNetworkId(md5(random_bytes(10)));
            $customer->setEmail(uniqid() . '@fake.email');
            $customer->setPassword($this->encoder->encodePassword($customer, uniqid()));

            $this->entityManager->persist($customer);
            $this->entityManager->flush();

            return $customer;
        } catch (OptimisticLockException $e) {
            dd($e->getMessage());
        } catch (ORMException $e) {
            dd($e->getMessage());
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}