<?php


namespace App\Services\Catalog;


use App\Repository\LocalityRepository;
use App\Repository\OwnershipTypeRepository;
use App\Repository\ProcurementFormatRepository;
use App\Repository\ProcurementMethodRepository;

class CatalogService
{
    private LocalityRepository $localityRepository;
    private ProcurementFormatRepository $procurementFormatRepository;
    private ProcurementMethodRepository $procurementMethodRepository;
    private OwnershipTypeRepository $ownershipTypeRepository;

    public function __construct(
        LocalityRepository $localityRepository,
        ProcurementFormatRepository $procurementFormatRepository,
        ProcurementMethodRepository $procurementMethodRepository,
        OwnershipTypeRepository $ownershipTypeRepository
    )
    {

        $this->localityRepository = $localityRepository;
        $this->procurementFormatRepository = $procurementFormatRepository;
        $this->procurementMethodRepository = $procurementMethodRepository;
        $this->ownershipTypeRepository = $ownershipTypeRepository;
    }

    public function getLocalities()
    {
        return $this->localityRepository->all();
    }

    public function getProcurementFormats()
    {
        return $this->procurementFormatRepository->all();
    }

    public function getProcurementMethods()
    {
        return $this->procurementMethodRepository->all();
    }

    public function getOwnershipTypes()
    {
        return $this->ownershipTypeRepository->all();
    }
}