<?php

declare(strict_types=1);

namespace App\Console;
use App\Entity\Lot;
use App\Entity\Requirement;
use App\Entity\Tender;
use App\Entity\TenderRequirement;
use App\Hydrator\LotHydrator;
use App\Hydrator\RequirementHydrator;
use App\Hydrator\TenderHydrator;
use App\Services\Parser\Assembler\GovKg\Model\Tender\LotItem;
use Doctrine\Persistence\ObjectRepository;
use \DetectLanguage\DetectLanguage;

use App\Entity\Locality;
use App\Entity\OwnershipType;
use App\Entity\Company;
use App\Entity\ProcurementFormat;
use App\Entity\ProcurementMethod;
use App\Enum\LockersEnum;


use App\Hydrator\CompanyHydrator;
use App\Hydrator\LocalityHydrator;
use App\Hydrator\OwnershipTypeHydrator;
use App\Hydrator\ProcurementFormatHydrator;
use App\Hydrator\ProcurementMethodHydrator;
use App\Services\Parser\Assembler\GovKg\Model\Tender\TenderItem;
use App\Services\Parser\Assembler\GovKg\Model\Tender\TenderList;
use App\Services\Parser\Assembler\GovKg\Model\Tender\Company as CompanyModel;
use App\Services\Parser\Assembler\GovKg\Model\Tender\Requirement as RequirementModel;
use App\Services\Parser\Facade\ParserFacade;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/** @see https://symfony.com/doc/current/components/lock.html */

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\LockInterface;
use Symfony\Component\Lock\Store\SemaphoreStore;

final class ParseGovKgCommand extends Command
{

    private LoggerInterface $logger;
    private ParserFacade $parserFacade;
    private EntityManagerInterface $entityManager;
    private $detectLangApiKey;

    public function __construct(LoggerInterface $logger, ParserFacade $parserFacade, EntityManagerInterface $entityManager, ContainerBagInterface $params)
    {
        parent::__construct();

        $this->logger = $logger;
        $this->parserFacade = $parserFacade;
        $this->entityManager = $entityManager;
        $this->detectLangApiKey = $params->get('parser')['lang_detect']['api_key'];
    }

    protected function configure(): void
    {
        $this->setName('parse:gov-kg')
            ->setDescription('Parsing last orders from gov.kg');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $lock = $this->getLock();

        if (false === $lock->acquire()) {
            return 0;
        }

        try {
            $tenderList = $this->parserFacade->parseGovKg();

            $this->saveData($tenderList, $output);

        } catch (Exception $exception) {
            $this->logger->error(self::class . ' catch exception: ' . $exception->getMessage(), (array)$exception);
        }

        $lock->release();

        return 0;
    }

    private function saveData(TenderList $tenderList, ConsoleOutput $output): void
    {
        /** @var TenderItem $tender */
        foreach ($tenderList as $tender) {

            $locality = $this->getOrCreateLocalityFromCompany($tender->getCompany()->getLocality());
            $ownershipType = $this->getOrCreateOwnershipTypeFromCompany($tender->getCompany()->getTypeOfOwnership());
            $company = $this->getOrCreateCompany($tender->getCompany(), $locality, $ownershipType);
            $procurementFormat = $this->getOrCreateProcurementFormat($tender->getPurchaseFormat());
            $procurementMethod = $this->getOrCreateProcurementMethod($tender->getPurchaseMethod());

            $tenderEntity = $this->getOrCreateTender($tender, $company, $procurementFormat, $procurementMethod);

            /** @var RequirementModel $requirement */
            foreach ($tender->getRequirementList() as $requirement)
            {
                $requirementEntity = $this->getOrCreateRequirement($requirement->getRequirement());

                $this->associateTenderWithRequirement($tenderEntity->getId(), $requirementEntity->getId(), $requirement->getDesc());
            }

            /** @var LotItem $lot */
            foreach ($tender->getLotList() as $lot)
            {
                $this->getOrCreateLot($tenderEntity->getId(), $lot);
            }

        }
    }

    private function associateTenderWithRequirement(int $tenderId, int $requirementId, string $desc): void
    {
        $repository = $this->getRepository(TenderRequirement::class);

        $tenderRequirementEntity = $repository->findOneBy(['tenderId' => $tenderId, 'requirementId' => $requirementId]);

        if (null === $tenderRequirementEntity)
        {
            $tenderRequirement =  new TenderRequirement($tenderId, $requirementId, $desc);

            $this->entityManager->persist($tenderRequirement);
            $this->entityManager->flush();
        }
    }

    private function getOrCreateRequirement(string $requirementTitle): Requirement
    {
        $repository = $this->getRepository(Requirement::class);
        $requirement = $repository->findOneBy(['title' => $requirementTitle]);

        if (null !== $requirement) {
            return $requirement;
        }

        $requirement = RequirementHydrator::hydrate($requirementTitle);

        $this->entityManager->persist($requirement);
        $this->entityManager->flush();


        return $requirement;
    }

    private function getOrCreateLot(int $tenderId, LotItem $lotItem): Lot
    {
        $repository = $this->getRepository(Lot::class);
        $lot = $repository->findOneBy(['tenderId' => $tenderId, 'number' => $lotItem->getNumber()]);

        if (null !== $lot) {
            return $lot;
        }

        $lot = LotHydrator::hydrate($lotItem, $tenderId);

        $this->entityManager->persist($lot);
        $this->entityManager->flush();

        return $lot;
    }

    private function getOrCreateTender(TenderItem $tender, Company $company, ProcurementFormat $procurementFormat, ProcurementMethod $procurementMethod): Tender
    {
        $repository = $this->getRepository(Tender::class);

        $tenderEntity = $repository->findOneBy(['number' => $tender->getId()]);

        if (null !== $tenderEntity) {
            return $tenderEntity;
        }

        DetectLanguage::setApiKey($this->detectLangApiKey);
        $tenderLang  = DetectLanguage::simpleDetect($tender->getTitle());

        $tenderEntity = TenderHydrator::hydrate($tender, $company, $procurementFormat, $procurementMethod, $tenderLang);

        $this->entityManager->persist($tenderEntity);
        $this->entityManager->flush();


        return $tenderEntity;

    }

    private function getOrCreateProcurementMethod(string $procurementMethod): ProcurementMethod
    {
        $repository = $this->getRepository(ProcurementMethod::class);

        $method = $repository->findOneBy(['title' => $procurementMethod]);

        if (null !== $method) {
            return $method;
        }

        $method = ProcurementMethodHydrator::hydrate($procurementMethod);

        $this->entityManager->persist($method);
        $this->entityManager->flush();

        return $method;
    }

    private function getOrCreateProcurementFormat(string $procurementFormat): ProcurementFormat
    {
        $repository = $this->getRepository(ProcurementFormat::class);

        $format = $repository->findOneBy(['title' => $procurementFormat]);

        if (null !== $format) {
            return $format;
        }

        $format = ProcurementFormatHydrator::hydrate($procurementFormat);

        $this->entityManager->persist($format);
        $this->entityManager->flush();

        return $format;
    }

    private function getOrCreateCompany(CompanyModel $company, Locality $locality, OwnershipType $ownershipType): Company
    {
        $repository = $this->getRepository(Company::class);

        $companyEntity = $repository->findOneBy(['name' => $company->getName(), 'inn' => $company->getInn()]);

        if (null !== $companyEntity) {
            return $companyEntity;
        }

        $companyEntity = CompanyHydrator::hydrate($company, $ownershipType, $locality);

        $this->entityManager->persist($companyEntity);
        $this->entityManager->flush();

        return $companyEntity;
    }

    private function getOrCreateOwnershipTypeFromCompany(string $ownerShipTypeStr): OwnershipType
    {
        $repository = $this->getRepository(OwnershipType::class);

        $ownershipType = $repository->findOneBy(['title' => $ownerShipTypeStr]);

        if (null !== $ownershipType) {
            return $ownershipType;
        }

        $ownershipType = OwnershipTypeHydrator::hydrate($ownerShipTypeStr);

        $this->entityManager->persist($ownershipType);
        $this->entityManager->flush();

        return $ownershipType;
    }

    private function getOrCreateLocalityFromCompany(string $localityStr): Locality
    {
        $repository = $this->getRepository(Locality::class);

        $locality = $repository->findOneBy(['title' => $localityStr]);

        if (null !== $locality) {
            return $locality;
        }

        $locality = LocalityHydrator::hydrate($localityStr);
        $this->entityManager->persist($locality);
        $this->entityManager->flush();

        return $locality;
    }

    private function getLock(): LockInterface
    {
        $factory = new LockFactory(new SemaphoreStore());

        return $factory->createLock(LockersEnum::PARSER_GOV_KG_COMMAND()->getValue());
    }

    private function getRepository(string $entity): ObjectRepository
    {
        return $this->entityManager->getRepository($entity);
    }
}