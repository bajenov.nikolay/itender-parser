<?php


namespace App\Common\Tools;

use Symfony\Component\HttpFoundation\JsonResponse;

class JsonResponseTool
{
    public static function success(array $data): JsonResponse
    {
        return new JsonResponse([
            'error' => false,
            'data' => $data
        ], 200);
    }

    public static function error(string $type, array $data, int $code): JsonResponse
    {
        return new JsonResponse([
            'error' => true,
            'type' => $type,  //ApiResponseTypes::VALIDATION(),
            'data' => $data
        ], $code);
    }
}