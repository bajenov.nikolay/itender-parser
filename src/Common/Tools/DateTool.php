<?php


namespace App\Common\Tools;


use DateTimeImmutable;
use Sentry\Client;

class DateTool
{
    public static function fromStringDateTimeImmutable(string $value, string $format = 'd F Y H:i'): ?DateTimeImmutable
    {

        try {
            $self = new self();
            return new DateTimeImmutable($self->replaceMonth($value));
        } catch (\Exception $e) {

        }


        return null;
    }

    public function replaceMonth(string $value): string
    {
        $from = [
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря',

        ];

        $to = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];

        return str_replace($from, $to, mb_strtolower(trim($value)));
    }


}