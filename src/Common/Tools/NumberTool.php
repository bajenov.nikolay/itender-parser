<?php


namespace App\Common\Tools;


class NumberTool
{
    public static function fromStringToFloat(string $value): float
    {
        return (float) filter_var(trim($value), FILTER_SANITIZE_NUMBER_FLOAT);
    }
}