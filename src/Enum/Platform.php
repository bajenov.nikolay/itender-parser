<?php


namespace App\Enum;


use MyCLabs\Enum\Enum;

class Platform extends Enum
{
    private const  ANDROID = 'android';
    private const  IOS = 'ios';

    public static function toArray()
    {
        return [
            self::ANDROID,
            self::IOS
        ];
    }
}