<?php


namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static static  PARSER_GOV_KG_COMMAND()
 */
class LockersEnum extends Enum
{

    private const  PARSER_GOV_KG_COMMAND = 'parser-gov-kg-command';
}