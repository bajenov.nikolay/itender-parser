<?php


namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static static  ANONYMOUS()
 * @method static static  ANONYMOUS_CODE()
 */
class SocialNetwork extends Enum
{
    private const ANONYMOUS = 'anonymous';
    private const ANONYMOUS_CODE = 1;

    public static function getCodes()
    {
        return [
            self::ANONYMOUS_CODE => self::ANONYMOUS,
        ];
    }

    public static function getLabels()
    {
        return [
            self::ANONYMOUS_CODE => 'Anonymous',
        ];
    }

    public static function getValueByCode(int $code): ?string
    {
        $labels = self::getLabels();

        return (isset($labels[$code])) ? $labels[$code] : null;
    }


}