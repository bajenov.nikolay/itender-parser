<?php


namespace App\Enum;


use MyCLabs\Enum\Enum;

/**
 * @method static static  VALIDATION()
 */
class ApiResponseTypes extends Enum
{
    private const  VALIDATION = 'validation';
}