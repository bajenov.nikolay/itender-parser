<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901082518 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE ownership_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE procurement_format_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE procurement_method_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE locality_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tender_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE lot_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE requirement_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE company_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE customers_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE ownership_type (id INT NOT NULL, title VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16DC78CB2B36786B ON ownership_type (title)');
        $this->addSql('CREATE TABLE procurement_format (id INT NOT NULL, title VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5456541C2B36786B ON procurement_format (title)');
        $this->addSql('CREATE TABLE procurement_method (id INT NOT NULL, title VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4B51CA32B36786B ON procurement_method (title)');
        $this->addSql('CREATE TABLE tender_requirement (tender_id INT NOT NULL, requirement_id INT NOT NULL, description TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(tender_id, requirement_id))');
        $this->addSql('CREATE TABLE locality (id INT NOT NULL, title VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E1D6B8E62B36786B ON locality (title)');
        $this->addSql('CREATE TABLE tender (id INT NOT NULL, number BIGINT NOT NULL, company_id INT NOT NULL, procurement_format_id INT NOT NULL, procurement_method_id INT NOT NULL, lang VARCHAR(2) NOT NULL, title VARCHAR(255) NOT NULL, estimated_amount NUMERIC(10, 0) NOT NULL, publish_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, need_to_apply TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_42057A772B36786B ON tender (title)');
        $this->addSql('CREATE INDEX tender_company_id_idx ON tender (company_id)');
        $this->addSql('CREATE INDEX tender_procurement_format_id_idx ON tender (procurement_format_id)');
        $this->addSql('CREATE INDEX tender_procurement_method_id_idx ON tender (procurement_method_id)');
        $this->addSql('COMMENT ON COLUMN tender.publish_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN tender.need_to_apply IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE lot (id INT NOT NULL, tender_id INT NOT NULL, number VARCHAR(10) NOT NULL, title VARCHAR(1000) NOT NULL, amount NUMERIC(10, 0) NOT NULL, delivery_address VARCHAR(255) DEFAULT NULL, delivery_conditions VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX tender_id_idx ON lot (tender_id)');
        $this->addSql('CREATE TABLE requirement (id INT NOT NULL, title VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DB3F55502B36786B ON requirement (title)');
        $this->addSql('CREATE TABLE company (id INT NOT NULL, ownership_type_id INT NOT NULL, locality_id INT NOT NULL, name VARCHAR(255) NOT NULL, inn BIGINT NOT NULL, address VARCHAR(512) NOT NULL, phone VARCHAR(256) NOT NULL, bank VARCHAR(256) NOT NULL, checking_account BIGINT NOT NULL, bik INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094F5E237E06 ON company (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FE93323CB ON company (inn)');
        $this->addSql('CREATE TABLE customers (id INT NOT NULL, social_network SMALLINT NOT NULL, social_network_id VARCHAR(128) NOT NULL, email VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, roles JSON DEFAULT NULL, api_token VARCHAR(255) DEFAULT NULL, platform VARCHAR(16) NOT NULL, push_token VARCHAR(180) DEFAULT NULL, device_id VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_62534E21E7927C74 ON customers (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_62534E217BA2F5EB ON customers (api_token)');
        $this->addSql('CREATE INDEX social_network_idx ON customers (social_network)');
        $this->addSql('CREATE INDEX social_network_id_idx ON customers (social_network_id)');
        $this->addSql('CREATE INDEX api_token_idx ON customers (api_token)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE ownership_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE procurement_format_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE procurement_method_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE locality_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tender_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE lot_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE requirement_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE company_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE customers_id_seq CASCADE');
        $this->addSql('DROP TABLE ownership_type');
        $this->addSql('DROP TABLE procurement_format');
        $this->addSql('DROP TABLE procurement_method');
        $this->addSql('DROP TABLE tender_requirement');
        $this->addSql('DROP TABLE locality');
        $this->addSql('DROP TABLE tender');
        $this->addSql('DROP TABLE lot');
        $this->addSql('DROP TABLE requirement');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE customers');
    }
}
